const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const STATIC = path.join(__dirname, 'app', 'static');
const TEMPLATE = path.join(__dirname, 'app', 'templates');

module.exports = {
    mode: 'development',
    context: path.join(STATIC, 'js'),
    plugins: [
        new HtmlWebpackPlugin({ 
            template: path.join(STATIC, 'html', 'base.html'),
            filename: path.join(TEMPLATE, 'base.html')
        })
    ],
    entry: './index.js',
    output: {
        path: path.join(path.join(STATIC, 'dist')),
        filename: 'index.[contentHash].bundle.js'
    }
};