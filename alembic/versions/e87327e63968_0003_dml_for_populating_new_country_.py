"""0003: DML for populating new country table

Revision ID: e87327e63968
Revises: e35c6d008931
Create Date: 2020-03-03 11:14:50.631143

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e87327e63968'
down_revision = 'e35c6d008931'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
    insert into public.country
    (
        code,
        name,
        local_name,
        government_form,
        head_of_state
    )

    select  code,
            name,
            localname,
            governmentform,
            headofstate
    from    public.holding_country
    order by code
    """)


def downgrade():
    op.execute("""
        truncate public.country restart identity cascade;
    """)
