"""0006: Create region table

Revision ID: f2fc2ed70d3d
Revises: 4000613693ac
Create Date: 2020-03-04 10:24:09.790789

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2fc2ed70d3d'
down_revision = '4000613693ac'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        create table if not exists public.region
        (
            id          serial primary key,
            name        text not null,
            created_at  timestamp default now(),
            updated_at  timestamp default now()
        )
    """)

    # Unique constraint on name
    op.execute("""
        alter table public.region
        add constraint uniq_region_name
        unique (name)
    """)

    op.execute("""
        insert into public.region ( name )

        select  distinct
                region
        from    public.holding_country
        order by region  
    """)

def downgrade():
    op.execute("""
        drop table if exists public.region
    """)
