"""0008: Add continent_id column to region

Revision ID: a77bf572de46
Revises: ca416972b2ba
Create Date: 2020-03-04 11:31:55.124624

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a77bf572de46'
down_revision = 'ca416972b2ba'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        alter table public.region
        add column if not exists
        continent_id integer
    """)

    op.execute("""
        with continents as 
        (
            select  r.id region_id,
                    c.id continent_id
            from    public.region r
            left join ( select distinct continent, region
                            from public.holding_country ) hc on hc.region = r.name
            left join continent c on c.name = hc.continent
        )
        update  public.region
        set     continent_id = c.continent_id
        from    continents c
        where   id = c.region_id
    """)

    # Set continent_id not null
    op.execute("""
        alter table public.region
        alter column continent_id set not null
    """)

    # FK constraint to continent id
    op.execute("""
        alter table public.region
        add constraint fk_region_continent_id
        foreign key (continent_id) references public.continent (id)
    """)


def downgrade():
    op.execute("""
        alter table public.region
        drop column if exists continent_id
    """)
