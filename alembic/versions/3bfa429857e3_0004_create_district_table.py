"""0004: Create district table

Revision ID: 3bfa429857e3
Revises: e87327e63968
Create Date: 2020-03-03 12:59:45.026405

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3bfa429857e3'
down_revision = 'e87327e63968'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        create table if not exists public.district
        (
            id          serial primary key,
            name        text not null,
            country_id  int not null,
            created_at  timestamp default now(),
            updated_at  timestamp default now()
        )
    """)

    # Unique constraint on name
    op.execute("""
        alter table public.district
        add constraint uniq_district_name_country_id
        unique (name, country_id)
    """)

    # Foreign key reference to country
    op.execute("""
        alter table public.district
        add constraint fk_district_country_id
        foreign key (country_id) references public.country (id)
    """)


def downgrade():
    op.execute("""
        drop table if exists public.district
    """)
