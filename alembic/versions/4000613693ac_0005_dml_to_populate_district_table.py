"""0005: DML to populate district table

Revision ID: 4000613693ac
Revises: 3bfa429857e3
Create Date: 2020-03-03 13:31:58.053519

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4000613693ac'
down_revision = '3bfa429857e3'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        insert into public.district
        (
            name,
            country_id
        )

        select  distinct
                holding_city.district,
                country.id
        from    public.holding_city
            left join public.country
              on public.country.code = public.holding_city.countrycode
        order by country.id,
                holding_city.district
    """)


def downgrade():
    op.execute("""
        truncate public.district restart identity cascade
    """)
