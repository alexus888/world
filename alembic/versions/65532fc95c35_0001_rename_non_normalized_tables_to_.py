"""0001: Rename non-normalized tables to 'holding' prefix

Revision ID: 65532fc95c35
Revises: 
Create Date: 2020-03-03 10:12:46.415994

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '65532fc95c35'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        alter table public.country
       rename to holding_country
    """)

    op.execute("""
        alter table public.city
       rename to holding_city
    """)

    op.execute("""
        alter table public.countrylanguage
       rename to holding_countrylanguage
    """)


def downgrade():
    op.execute("""
        alter table if exists public.holding_country
       rename to country
    """)

    op.execute("""
        alter table if exists public.holding_city
       rename to city
    """)

    op.execute("""
        alter table if exists public.holding_countrylanguage
       rename to countrylanguage
    """)
