"""0002: Create new country table

Revision ID: e35c6d008931
Revises: 65532fc95c35
Create Date: 2020-03-03 10:54:06.771518

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e35c6d008931'
down_revision = '65532fc95c35'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        create table if not exists public.country
        (
            id              serial primary key,
            code            text not null,
            name            text not null,
            local_name      text,
            government_form text,
            head_of_state   text,
            created_at      timestamp default now(),
            updated_at      timestamp default now()
        ) 
    """)

    # Unique constraint on name
    op.execute("""
        alter table public.country 
        add constraint uniq_country_name
        unique (name)
    """)

    # Unique constraint on code
    op.execute("""
        alter table public.country 
        add constraint uniq_country_code
        unique (code)
    """)


def downgrade():
    op.execute("""
        drop table if exists public.country
    """)
