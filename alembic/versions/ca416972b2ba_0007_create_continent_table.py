"""0007: Create continent table

Revision ID: ca416972b2ba
Revises: f2fc2ed70d3d
Create Date: 2020-03-04 11:17:12.037808

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ca416972b2ba'
down_revision = 'f2fc2ed70d3d'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("""
        create table if not exists public.continent
        (
            id serial primary key,
            name text not null,
            created_at timestamp default now(),
            updated_at timestamp default now()
        )
    """)

    op.execute("""
        alter table public.continent
        add constraint uniq_continent_name
        unique (name)
    """)

    op.execute("""
        insert into public.continent ( name )
        select  distinct
                continent
        from    public.holding_country
        order by continent
    """)

def downgrade():
    op.execute("""
        drop table if exists public.continent
    """)
