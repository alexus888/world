# World

World is a port of the example database available for MySQL on the mysql.com
website.  

It is a simple 1:1 port and no attempt was made to redesign the schema to
better suit PostgreSQL. Even more, the data itself starts as fairly "dirty".
It was not normalized, and this repo utilizes alembic and a series of migrations
to move it closer to normal.  


# Future State
  * Make moves to "flatten" migrations to a seed file, iteratively  
  * Make moves towards using an ORM and python models as the migrations mature