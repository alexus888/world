from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import create_engine
from dotenv import load_dotenv
import os

from app import app

load_dotenv()

engine = create_engine(app.config['DB_URL'])
db = scoped_session(sessionmaker(bind=engine))