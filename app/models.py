from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, ForeignKey


Base = declarative_base()

class Continent(Base):
    __tablename__ = 'continent'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
