from flask import render_template, jsonify

from app import app
from app.database import db
from app.models import Continent
from app.schemas import continents_schema

from pprint import pprint


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/continents', methods=['GET'])
def get():
    continents = db.query(Continent).all()
    return jsonify(continents_schema.dump(continents))
