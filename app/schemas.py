from flask_marshmallow import Marshmallow
from app import app


ma = Marshmallow(app)

class ContinentSchema(ma.Schema):
    class Meta:
        fields = ("id", "name")


continent_schema = ContinentSchema()
continents_schema = ContinentSchema(many=True)
